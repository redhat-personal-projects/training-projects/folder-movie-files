#!/usr/bin/env node

const path = require("path");
const fs = require("fs");
const fsp = require("fs/promises");
const mv = require("mv");

console.log("Movie Arranger - version 2.1.1");

function formatFolderNames(name) {
  return name
    .replaceAll("_", " ")
    .replaceAll(".", " ")
    .replaceAll(/( (720p|1080p|10bit).*)/g, "")
    .replace(/ \d{4}$/, " -$&");
}

function move(oldPath, newPath, callback) {
  return new Promise((resolve, reject) => {
    mv(oldPath, newPath, { mkdirp: true }, (err) => {
      if (err) reject(err);
      resolve();
    });
  });
}

function putInBox(
  str,
  boxOption = {
    padding: 0,
    margine: 0,
    borderStyle: "round",
    borderColor: "green",
    backgroundColor: "#552555",
  }
) {
  const chalk = require("chalk");
  const boxen = require("boxen");

  const greeting = chalk.white.bold(str);

  const msgBox = boxen(greeting, boxOption);

  console.log(msgBox);
}

function readAndMoveFiles() {
  const wts = process.cwd();
  let movedFileNumber = 0;
  return new Promise(async (resolve, reject) => {
    const files = await fsp
      .readdir(wts, { withFileTypes: true })
      .catch((err) => reject("Unable to scan directory: " + err));

    for (let file of files) {
      let fileWithoutEXT = path.basename(file.name, path.extname(file.name));
      if (
        file.name != "index.js" &&
        file.name != "desktop.ini" &&
        path.extname(file.name).length > 0 &&
        path.extname(file.name) != ".zip" &&
        path.extname(file.name) != ".rar" &&
        path.extname(file.name) != ".srt"
      ) {
        const formattedMovieName = formatFolderNames(fileWithoutEXT);
        const newFolderDir = path.join(wts, formattedMovieName);

        await move(
          path.join(wts, file.name),
          path.join(newFolderDir, file.name)
        ).catch((err) => {
          reject(err);
          putInBox(`<${file.name}> FAILED ⛔ `, {
            padding: 0,
            margine: 0,
            borderStyle: "round",
            borderColor: "red",
            backgroundColor: "#552555",
          });
        });
        putInBox(`<${formattedMovieName}> DONE ✔️ `);
        movedFileNumber++;
      }
    }

    resolve(movedFileNumber);
  });
}

readAndMoveFiles()
  .then((movedFileNumber) => {
    putInBox(
      `Number of moved files : ${movedFileNumber} 📝 `,
      (boxOption = {
        padding: 0,
        margine: 0,
        borderStyle: "round",
        borderColor: "#073A4B",
        backgroundColor: "#0CC2FF",
      })
    );
  })
  .catch((err) => console.error(err));
